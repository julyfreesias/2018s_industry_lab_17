package ictgradschool.industry.testingandrefactoring.ex02;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TDDTest {

    private TDD tdd;


    @Before
   public void setUp (){ tdd = new TDD();}

   @Test
   public void convertTest (){

        assertEquals(3.4,tdd.convertStringToDouble("3.4"), 0.5);

   }

   @Test
    public void areaOfRectangleTest() {

        assertEquals(379.25, TDD.areaOfRectangle(20.5,18.5),0.5);
   }

   @Test
    public void areaOfCircleTest(){

        assertEquals(1320.00, TDD.areaOfCircle(20.5),0.5);
   }

   @Test
    public void roundingArea(){

        assertEquals(8, TDD.roundingArea(7.5),0.5);
   }

   @Test
    public void smallerArea () {

        assertEquals(34.2, TDD.smallerArea(34.2,54.3),0.5);
   }
}
