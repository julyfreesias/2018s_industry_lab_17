package ictgradschool.industry.testingandrefactoring.ex01;

import org.junit.Before;
import org.junit.Test;
import org.omg.CORBA.PUBLIC_MEMBER;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }
    @Test
    public void testIllegalBackTrackToSouth () {
           myRobot.turn();
           myRobot.turn();
           assertEquals(Robot.Direction.South, myRobot.currentState().direction);
           assertEquals(1, myRobot.column());
        assertEquals(10, myRobot.row());
           myRobot.backTrack();
           assertEquals(Robot.Direction.East, myRobot.currentState().direction);
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());

    }


//    public void testIllegalBackTrackToWest () {
//        try {
//            myRobot.turn();
//            myRobot.turn();
//            assertEquals(Robot.Direction.West, myRobot.currentState().direction);
//            myRobot.backTrack();
//            fail();
//        }catch (IllegalMoveException e){
//            assertEquals(1,myRobot.currentState().column);
//        }
//    }


    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveSouth() {
        boolean atBottom = false;
        try {
            // Move the robot to the bottom row.
            myRobot.turn();
            myRobot.turn();
            assertEquals(Robot.Direction.South,myRobot.getDirection());
            myRobot.move();
            fail();
            // Check that robot has reached the top.
//            atBottom = myRobot.currentState().row == 10;
//            assertTrue(atBottom);
        } catch (IllegalMoveException e) {
            assertEquals(10,myRobot.currentState().row);
        }


    }


    @Test
    public void testIllegalMoveWest() {
        boolean atWest = false;
        try {
            myRobot.turn();
            myRobot.turn();
            myRobot.turn();
            assertEquals(Robot.Direction.West,myRobot.getDirection());
            myRobot.move();
            fail();

        } catch (IllegalMoveException e) {
            assertEquals(1,myRobot.currentState().column);
        }

    }


    @Test
    public void testIllegalMoveEast() {
        boolean atEast = false;
        try {
            myRobot.turn();
            for (int i = 0; i < Robot.GRID_SIZE-1; i++) {
                myRobot.move();
            }
            atEast = myRobot.currentState().column == 10;
            assertTrue(atEast);

        } catch (IllegalMoveException e) {
            fail();
        }
        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().column);
        }

    }




}
