package ictgradschool.industry.testingandrefactoring.ex03;

import java.io.*;
import java.util.*;

/**
 * TODO Please test & refactor this - my eyes are watering just looking at it :'(
 *
 *
 * 1. What does the program do?
 * 2. What tests to I need to create to ensure that the program behaviour is still the same after refactoring?
 * 3. Is there any duplicated code?
 * 4. Are there any long methods?
 * 5. Are the variable names meaningful?
 * 6. What are the refactoring functions from my IDE that I can use to ease my refactoring job?
 *
 * Make a note of what you’ve done to accomplish this exercise. In particular, note:
 * how you have refactored the program
 * what unit tests you have created, and
 * the IDE functions you’ve used which have proved useful to you.
 *
 * take out instance variables from main and make constructor ??
 * separate implements in main() into separate methods
 * try/catch for IOException
 *
 */
public class ExcelNew {

	private String line =null;
	private String output = null;
	private int classSize =550;
	ArrayList<String> firstNameList = new ArrayList<String>();
	ArrayList<String> surnameList = new ArrayList<String>();
	private String student =null;

	//execute methods.
	public void ExcelNew() throws IOException {
		readFile();
		output=studentMarksGenerator();
		createFile(output);

	}

	//firstly read File for firstName, surname.
	//surround try/catch for each reader.
	//can this reduce the redundancy for try/catch??

	public void readFile ()  {

		/*reads two text files, FirstNames.txt and Surnames.txt*/

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader("FirstNames.txt"));
			while((line = br.readLine())!= null){
				firstNameList.add(line);
				br.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}



		try {
			br = new BufferedReader(new FileReader("Surnames.txt"));
			while((line = br.readLine())!= null){
				surnameList.add(line);
				br.close();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}



	}



	/*generates random marks for a randomly generated list of students*/
	public String studentMarksGenerator(){

		for(int i = 1; i <= classSize; i++) {
			String student = "";
			if (i / 10 < 1) {
				student += "000" + i;
			} else if (i / 100 < 1) {
				student += "00" + i;
			} else if (i / 1000 < 1) {
				student += "0" + i;
			} else {
				student += i;
			}
			int randFNIndex = (int) (Math.random() * firstNameList.size());
			int randSNIndex = (int) (Math.random() * surnameList.size());
			student += "\t" + surnameList.get(randSNIndex) + "\t" + firstNameList.get(randFNIndex) + "\t";


			int randStudentSkill = randStudentSkill();

			//method call for each marks generator with randomly generated number.

			labMark(randStudentSkill);

			testMark(randStudentSkill);

			examMark(randStudentSkill);

			student += "\n";
			output += student;


		}return output;

	}

	public int randStudentSkill() {
		return (int)(Math.random()*101);

	}


	public void labMark(int randStudentSkill){
		int numLabs = 3;
		for(int j = 0; j < numLabs; j++){
			if(randStudentSkill <= 5){
				student += (int)(Math.random()*40); //[0,39]
			}else if ((randStudentSkill > 5) && (randStudentSkill <= 15)){
				student += ((int)(Math.random()*10) + 40); // [40,49]
			}else if((randStudentSkill > 15) && (randStudentSkill <= 25)){
				student += ((int)(Math.random()*20) + 50); // [50,69]
			}else if((randStudentSkill > 25) && (randStudentSkill <= 65)){
				student += ((int)(Math.random()*20) + 70); // [70,89]
			} else{
				student += ((int)(Math.random()*11) + 90); //[90,100]
			}
			student += "\t";
		}

	}
	public void testMark(int randStudentSkill){
		if(randStudentSkill <= 5){
			student += (int)(Math.random()*40); //[0,39]
		}else if((randStudentSkill > 5) && (randStudentSkill <= 20)){
			student += ((int)(Math.random()*10) + 40); //[40,49]
		} else if((randStudentSkill > 20) && (randStudentSkill <= 65)){
			student += ((int)(Math.random()*20) + 50); //[50,69]
		} else if((randStudentSkill > 65) && (randStudentSkill <= 90)){
			student += ((int)(Math.random()*20) + 70); //[70,89]
		} else{
			student += ((int)(Math.random()*11) + 90); //[90,100]
		}
		student += "\t";
	}

	public void examMark(int randStudentSkill){
		if(randStudentSkill <= 7){
			int randDNSProb = (int)(Math.random()*101);
			if(randDNSProb <= 5){
				student += ""; //DNS
			}else{
				student += (int)(Math.random()*40); //[0,39]
			}
		} else if((randStudentSkill > 7) && (randStudentSkill <= 20)){
			student += ((int)(Math.random()*10) + 40); //[40,49]
		} else if((randStudentSkill > 20) && (randStudentSkill <= 60)){
			student += ((int)(Math.random()*20) + 50);//[50,69]
		} else if((randStudentSkill > 60) && (randStudentSkill <= 90)){
			student += ((int)(Math.random()*20) + 70); //[70,89]
		} else{
			student += ((int)(Math.random()*11) + 90); //[90,100]
		}
	}



	//finally write the data and generate new data file.
	//surrounded by try/catch for exception.
	public void createFile(String output) {
		/*creates a text file, Data_out.txt*/
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter("Data_Out.txt"));
			bw.write(output);
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}




	public static void main(String [] args) throws IOException {

		ExcelNew ex = new ExcelNew();
		ex.ExcelNew();

	}

}

