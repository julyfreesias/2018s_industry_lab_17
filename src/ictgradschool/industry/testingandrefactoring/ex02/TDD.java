package ictgradschool.industry.testingandrefactoring.ex02;

import ictgradschool.Keyboard;

public class TDD {


   
    
    public static void main(String[] args) {

        System.out.println("Welcome to Shape Area Calculator!");

        System.out.println("Enter the width of the rectangle :");
        String userInput = Keyboard.readInput();
        Double userWidth= convertStringToDouble(userInput);


        System.out.println("Enter the length of the rectangle :");
        userInput = Keyboard.readInput();
        Double userLength= convertStringToDouble(userInput);


        System.out.println("Enter the radius");
        userInput = Keyboard.readInput();
        Double userRadius = convertStringToDouble(userInput);

        System.out.println("The radius of the Circle is : " + userRadius );


        double areaC =areaOfCircle(userRadius);
        System.out.println("Area of Circle is :" + areaC);

        double arerL = areaOfRectangle(userWidth,userLength);
        System.out.println("Area of Rectangle  is :" + arerL);


        double smallA = smallerArea(areaC,arerL);
        System.out.println("The smaller area is :" + smallA);


    }

    public static double convertStringToDouble(String userInput) {
        Double DoubleduserInput = Double.parseDouble(userInput);
       return DoubleduserInput;
    }


    public static double areaOfRectangle(double userWidth, double userLength) {

          double areaOfRectangle;
          areaOfRectangle = userWidth * userLength;

        return areaOfRectangle;
    }

    public static double areaOfCircle(double userRadius) {

        double areaOfCircle;
        areaOfCircle = Math.PI * Math.pow(userRadius, 2.0);

        return  areaOfCircle;
    }

    public static double roundingArea(double v) {

        double roundedArea = Math.round(v);

        return roundedArea;
    }

    public static double smallerArea(double areaOfRectangle, double areaOfCircle) {
        double smallerArea=0;
        smallerArea = Math.min(areaOfRectangle, areaOfCircle);
        return smallerArea;
    }



}
